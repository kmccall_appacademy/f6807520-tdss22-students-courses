## Student
# * `Student#initialize` should take a first and last name.
# * `Student#name` should return the concatenation of the student's
#   first and last name.
# * `Student#courses` should return a list of the `Course`s in which
#   the student is enrolled.
# * `Student#enroll` should take a `Course` object, add it to the
#   student's list of courses, and update the `Course`'s list of
#   enrolled students.
#     * `enroll` should ignore attempts to re-enroll a student.
# * `Student#course_load` should return a hash of departments to # of
#   credits the student is taking in that department.


class Student
  attr_accessor :courses, :first_name, :last_name

  def initialize (first_name, last_name)
    @first_name = first_name.capitalize
    @last_name = last_name.capitalize
    @courses = []
  end

  def name
    "#{first_name} #{last_name}"
  end

  def enroll(course)
    return if courses.include?(course)
    if conflicting?(course)
      raise "ERROR: This course conflicts with your schedule..."
    end
    self.courses << course
    course.students << self unless course.students.include?(self)
  end

  def course_load
    c_load = Hash.new(0)
    self.courses.each do |course|
      c_load[course.department] += course.credits
    end
    c_load
  end

  def conflicting?(second_course)
    self.courses.any? do |enrolled_course|
      second_course.conflicts_with?(enrolled_course)
    end
  end

end
